 **# 短剧系统搭建微信抖音h5公众号app看剧系统** 

#### 介绍
短剧系统简单来说就是用来看短剧的小程序。系统功能简介：分销功能，一键导入；vip会员，自定改价，卡密激活，数据统计
支持五端：抖音，微信小程序，公众号，h5, 双端app。

#### 商用提示
获取完整搭建商用请联系官方获取授权： wx：ywxs5787


#### 功能特点

短剧播放，VIP收费，充值观看，短剧收藏，自动切换，积分套餐，自由选集，二级分销裂变，对接流量主变现，支持创作者入驻，PC独立后台管理，壁纸，表情包下载，内容管理分类，专题分类，可单次付费或月会员，会员开通与支付功能。

1.微信小程序，h5，抖音小程序，app，公众号。
2.可以自定义会员价、折扣。或余额充值功能，可以设置剧集免费试看付费价格，单一集或整集价已接腾讯云点播，短剧播放更流畅，不占用服务器资源。
3.部署好小程序环境及前端编译发布，支持代理分销短剧代理有专属的分销链接，后台自定义上传短剧多人员可以分配不同的权限管理后台。
4.分销订单，vip订单，积分订单，提现订单总量统计
5.提现金额、佣金统计
6.剧集销量排名、销售额排名。


#### 页面截图

![输入图片说明](%E7%9F%AD%E5%89%A7%E7%B3%BB%E7%BB%9F%E9%85%8D%E5%9B%BE1.png)

![输入图片说明](%E7%9F%AD%E5%89%A7%E7%B3%BB%E7%BB%9F%E9%A1%B5%E9%9D%A22.png)

![输入图片说明](%E7%9F%AD%E5%89%A7%E7%B3%BB%E7%BB%9F%E5%90%8E%E7%AB%AFPC%E7%AB%AF1.png)

获取后端体验链接请联系开发者： wx：ywxs5787

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 更新日志

## [1.1.1] 2023-09-12

### 新增 微信小程序流量主激励视频广告功能
### 新增 微信小程序隐私协议弹窗

## [1.1.0] 2023-09-02

### 优化 视频播放页面充值卡片
### 修改 登录授权后返回到发起授权页面

## [1.0.4] 2023-08-31

### 新增 支持微信H5和微信小程序横屏视频观看

## [1.0.3] 2023-08-21

### 新增 微信H5剧集页面自定义分享
### 新增 剧集页面点播提示信息

## [1.0.2] 2023-08-19

### 新增 视频播放页，视频播放错误信息展示
### 新增 二级页面顶部导航返回首页图标按钮
### 新增 安卓端微信H5视频是否自动播放开关，可自行在后台配置
### 修复 视频播放页，一直加载中无法操作问题
### 修复 app端更新不显示进度问题
### 修复 个人中心页面无法回到顶部问题
